package com.harmonycloud;

import com.csvreader.CsvReader;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ReadCsvFile {

    public static List<Alarm> getAlarmFile(String filePath) {
        List<Alarm> list = new ArrayList<>();
        try {
            DataInputStream in = new DataInputStream(new FileInputStream(new File(filePath)));
            CsvReader csvReader = new CsvReader(in, Charset.forName("GBK"));
            csvReader.readHeaders();
            while (csvReader.readRecord()) {
                Alarm alarm = new Alarm();
                alarm.setHappendModel(csvReader.get("数据来源").replace("SPV=业务监控平台 : 组件=", ""));
                alarm.setAlarmContent(csvReader.get("事件"));
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                alarm.setAlarmDetail(csvReader.get("事件"));
                alarm.setType(1);
                if (!"".equals(csvReader.get("最后发生时间"))) {
                    alarm.setHappendTime(sdf.parse(csvReader.get("最后发生时间").replace("/", "-") + ":00"));
                    list.add(alarm);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return list;
    }

    public static List<List<String>> getAlarmPreData(List<Alarm> sortByTimeAlarms, int timesize) {
        List<List<String>> list = new ArrayList<>();
        while (!sortByTimeAlarms.isEmpty()) {
            Alarm fisrtAlarm = sortByTimeAlarms.get(0);
            sortByTimeAlarms.remove(0);
            List<String> templist = getOneTimeWindowsAlarm(sortByTimeAlarms, fisrtAlarm.getHappendTime(), timesize);
            if (!templist.isEmpty()) {
                if (!list.contains(templist))
                    list.add(templist);
            }
        }
        return list;
    }


    public static List<String> getOneTimeWindowsAlarm(List<Alarm> sortedAlarms, Date firstTime, int timeWindowsSize) {

        List<String> oneTimeWindowsAlarms = new ArrayList<>();
        List<Alarm> alarmList = new ArrayList<>();
        if (sortedAlarms == null) {
            return oneTimeWindowsAlarms;
        }

        for (Alarm alarm : sortedAlarms) {
            if ((alarm.getHappendTime().getTime() - firstTime.getTime()) <= timeWindowsSize) {
                if (null == alarm.getId() && !oneTimeWindowsAlarms.contains(alarm.getHappendModel())) {
                    /*if (alarmList.size() == 0 || null != alarmList.get(0).getId()) {
                        oneTimeWindowsAlarms.add(alarm.getHappendModel());
                        alarmList.add(alarm);
                    }*/oneTimeWindowsAlarms.add(alarm.getHappendModel());
                } else if (!oneTimeWindowsAlarms.contains(alarm.getAlarmContent())) {
                    /*if (alarmList.size() == 0 || null == alarmList.get(0).getId()) {
                        oneTimeWindowsAlarms.add(alarm.getAlarmContent());
                        alarmList.add(alarm);
                    }*/
                    oneTimeWindowsAlarms.add(alarm.getAlarmContent());
                }

            }
        }

        return oneTimeWindowsAlarms;
    }
}
