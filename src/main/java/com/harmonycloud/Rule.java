package com.harmonycloud;

public class Rule {

    private Integer id;

    private String appName;

    /**
     * 前项集合
     */
    private String antecedent;

    /**
     * 后项集合
     */
    private String consequent;

    private String confidence;

    private Integer appmodelId;

    public Rule(Integer appmodelId, String antecedent, String consequent, String confidence) {
        this.appmodelId = appmodelId;
        this.antecedent = antecedent;
        this.consequent = consequent;
        this.confidence = confidence;
    }

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return app_name
     */
    public String getAppName() {
        return appName;
    }

    /**
     * @param appName
     */
    public void setAppName(String appName) {
        this.appName = appName;
    }

    /**
     * 获取前项集合
     *
     * @return antecedent - 前项集合
     */
    public String getAntecedent() {
        return antecedent;
    }

    /**
     * 设置前项集合
     *
     * @param antecedent 前项集合
     */
    public void setAntecedent(String antecedent) {
        this.antecedent = antecedent;
    }

    /**
     * 获取后项集合
     *
     * @return consequent - 后项集合
     */
    public String getConsequent() {
        return consequent;
    }

    /**
     * 设置后项集合
     *
     * @param consequent 后项集合
     */
    public void setConsequent(String consequent) {
        this.consequent = consequent;
    }

    /**
     * @return confidence
     */
    public String getConfidence() {
        return confidence;
    }

    /**
     * @param confidence
     */
    public void setConfidence(String confidence) {
        this.confidence = confidence;
    }

    public Integer getAppmodelId() {
        return appmodelId;
    }

    public void setAppmodelId(Integer appmodelId) {
        this.appmodelId = appmodelId;
    }
}