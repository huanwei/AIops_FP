package com.harmonycloud;


public class ErrorRelated {

    private Integer id;

    private String antecedent;

    private String consequent;

    private Integer cnt;

    private String confidence;

    private Integer realDelete;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return antecedent
     */
    public String getAntecedent() {
        return antecedent;
    }

    /**
     * @param antecedent
     */
    public void setAntecedent(String antecedent) {
        this.antecedent = antecedent;
    }

    /**
     * @return consequent
     */
    public String getConsequent() {
        return consequent;
    }

    /**
     * @param consequent
     */
    public void setConsequent(String consequent) {
        this.consequent = consequent;
    }

    /**
     * @return cnt
     */
    public Integer getCnt() {
        return cnt;
    }

    /**
     * @param cnt
     */
    public void setCnt(Integer cnt) {
        this.cnt = cnt;
    }

    /**
     * @return confidence
     */
    public String getConfidence() {
        return confidence;
    }

    /**
     * @param confidence
     */
    public void setConfidence(String confidence) {
        this.confidence = confidence;
    }

    public Integer getRealDelete() {
        return realDelete;
    }

    public void setRealDelete(Integer realDelete) {
        this.realDelete = realDelete;
    }
}