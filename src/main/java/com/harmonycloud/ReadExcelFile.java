package com.harmonycloud;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class ReadExcelFile {
    public static List<Alarm> getAlarmFromFile(String filePath, int ignoreRows) {

        if (filePath == null) {
            return null;
        }

        Workbook wb;
        InputStream is;
        List<Alarm> alarms = new ArrayList<>();
        try {
            is = new FileInputStream(filePath);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            wb = WorkbookFactory.create(is);

            Sheet sheet = wb.getSheetAt(0);
            // 得到总行数
            int rowNum = sheet.getLastRowNum();
            for (int i = ignoreRows; i <= rowNum; i++) {
                Row row = sheet.getRow(i);
                String module = row.getCell(3).toString();
                Date time = null;
                if (!row.getCell(5).toString().equals("")) {
                    if (row.getCell(5).getCellType() == Cell.CELL_TYPE_NUMERIC)
                        time = row.getCell(5).getDateCellValue();
                    if (row.getCell(5).getCellType() == Cell.CELL_TYPE_STRING)
                        time = sdf.parse(row.getCell(5).toString());
                }
                String content = row.getCell(3).toString();
                String details = row.getCell(3).toString();
                Alarm alarm = new Alarm(productAlarmID(), module, time, content, details);
                if (!alarm.getHappendModel().equals("") && !alarm.getHappendModel().equals("无"))
                    alarms.add(alarm);
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return alarms;
    }

    public static String productAlarmID() {
        String str = UUID.randomUUID().toString().replace("-", "");
        return str;
    }

    public static void Array2CSV(List<Alarm> data, String path) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path), "UTF-8"));
            for (int i = 0; i < data.size(); i++) {
                Alarm onerow = data.get(i);
                out.write(sdf.format(onerow.getHappendTime()));
                out.write(",");
                out.write(onerow.getHappendModel());
                out.write(",");
                out.write(onerow.getAlarmDetail());
                out.write(",");
                out.newLine();
            }
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        String data_path = "C:\\Users\\书友\\Desktop\\监控告警事件查询报表9.11A.xlsx";
        List<Alarm> list = ReadExcelFile.getAlarmFromFile(data_path, 3);
        String str = "C:\\Users\\书友\\Desktop\\智能运维\\Groceries.csv";
        ReadExcelFile.Array2CSV(list, str);
        System.out.println(list.size());
    }
}
