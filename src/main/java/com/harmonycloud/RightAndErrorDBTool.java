package com.harmonycloud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RightAndErrorDBTool {


    public List<ErrorRelated> selectErrorRelatedList() throws SQLException {

        List<ErrorRelated> errorRelateds = new ArrayList<>();
        Connection cn;
        PreparedStatement ps;
        cn = DBConnectionPool.getInstance().getConnection();
        ResultSet rs;
        String sql_query = "select * from t_error_related";
        ps = cn.prepareStatement(sql_query);
        rs = ps.executeQuery();
        while (rs.next()) {
            ErrorRelated errorRelated = new ErrorRelated();
            errorRelated.setId(rs.getInt("id"));
            errorRelated.setAntecedent(rs.getString("antecedent"));
            errorRelated.setConsequent(rs.getString("consequent"));
            errorRelated.setCnt(rs.getInt("cnt"));
            errorRelateds.add(errorRelated);
        }
        if (ps != null)
            ps.close();
        if (cn != null)
            cn.close();

        return errorRelateds;
    }

    public int updateErrorRelated(Integer id) throws SQLException {

        Connection cn;
        PreparedStatement ps;
        int result = 0;

        cn = DBConnectionPool.getInstance().getConnection();
        String sql_update = "UPDATE t_error_related SET cnt=cnt+1 WHERE id=" + id;
        ps = cn.prepareStatement(sql_update);
        result = ps.executeUpdate();

        if (ps != null)
            ps.close();
        if (cn != null)
            cn.close();
        return result;
    }

    public List<RightRelated> selectRightRelatedList() throws SQLException {

        List<RightRelated> errorRelateds = new ArrayList<>();
        Connection cn;
        PreparedStatement ps;
        cn = DBConnectionPool.getInstance().getConnection();
        ResultSet rs;
        String sql_query = "select * from t_right_related";
        ps = cn.prepareStatement(sql_query);
        rs = ps.executeQuery();
        while (rs.next()) {
            RightRelated rightRelated = new RightRelated();
            rightRelated.setId(rs.getInt("id"));
            rightRelated.setAntecedent(rs.getString("antecedent"));
            rightRelated.setConsequent(rs.getString("consequent"));
            rightRelated.setCnt(rs.getInt("cnt"));
            errorRelateds.add(rightRelated);
        }
        if (ps != null)
            ps.close();
        if (cn != null)
            cn.close();

        return errorRelateds;
    }

    public int updateRightRelated(Integer id) throws SQLException {

        Connection cn;
        PreparedStatement ps;
        int result = 0;

        cn = DBConnectionPool.getInstance().getConnection();
        String sql_update = "UPDATE t_right_related SET cnt=cnt+1 WHERE id=" + id;
        ps = cn.prepareStatement(sql_update);
        result = ps.executeUpdate();

        if (ps != null)
            ps.close();
        if (cn != null)
            cn.close();
        return result;
    }
}
