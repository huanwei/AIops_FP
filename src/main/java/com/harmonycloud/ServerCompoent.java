package com.harmonycloud;

public class ServerCompoent {

    private Integer id;

    private String serer;

    private String compoent;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerer() {
        return serer;
    }

    public void setSerer(String serer) {
        this.serer = serer;
    }

    public String getCompoent() {
        return compoent;
    }

    public void setCompoent(String compoent) {
        this.compoent = compoent;
    }
}
