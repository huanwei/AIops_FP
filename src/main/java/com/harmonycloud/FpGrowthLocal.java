package com.harmonycloud;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.mllib.fpm.AssociationRules;
import org.apache.spark.mllib.fpm.FPGrowth;
import org.apache.spark.mllib.fpm.FPGrowthModel;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class FpGrowthLocal {
    public static void main(String[] args) {

        double minSupport = 0.005;//最小支持度
        double minConfidence = 0.6;//最小置信度


        int numPartition = 3;  //数据分区
        /*if (args.length < 1) {
            System.out.println("<input data_path>");
            System.exit(-1);
        }*/
        String data_path = "C:\\Users\\书友\\Desktop\\服务器告警事件.xlsx";
        String data_path1 = "C:\\Users\\书友\\Desktop\\BPC - 副本.csv";
        if (args.length >= 2)
            minSupport = Double.parseDouble(args[1]);
        if (args.length >= 3)
            numPartition = Integer.parseInt(args[2]);
        if (args.length >= 4)
            minConfidence = Double.parseDouble(args[3]);
        int timesize = 1 * 60 * 1000;
        Integer appmodelId = 47;

        SparkConf conf = new SparkConf().setAppName("FPDemo").setMaster("local");////修改的地方
        JavaSparkContext sc = new JavaSparkContext(conf);
        List<Alarm> filedatabpc = ReadCsvFile.getAlarmFile(data_path1);
        DbStoreHelper dbStoreHelper = new DbStoreHelper();
        List<Alarm> filedata = null;
        try {
            filedata = dbStoreHelper.selectByAppmodelId(45);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //List<Alarm> filedata = ReadExcelFile.getAlarmFromFile(data_path, 3);
        filedata.addAll(filedatabpc);
        Collections.sort(filedata, Comparator.comparing(Alarm::getHappendTime));
        List<List<String>> preData = ReadCsvFile.getAlarmPreData(filedata, timesize);
        preData.forEach(sub -> {
            sub.forEach(str -> System.out.print(str + "***"));
            System.out.println("%%%%");
        });
        //加载数据
        JavaRDD<List<String>> transactions = sc.parallelize(preData);
        //创建FPGrowth的算法实例，同时设置好训练时的最小支持度和数据分区
        FPGrowth fpGrowth = new FPGrowth().setMinSupport(minSupport).setNumPartitions(numPartition);
        FPGrowthModel<String> model = fpGrowth.run(transactions);//执行算法

        //查看所有频繁諅，并列出它出现的次数
        /*for (FPGrowth.FreqItemset<String> itemset : model.freqItemsets().toJavaRDD().collect())
          System.out.println("[" + itemset.javaItems() + "]," + itemset.freq());*/
        //通过置信度筛选出强规则
        //antecedent表示前项、consequent表示后项、confidence表示规则的置信度
        List<Rule> ruleList = new ArrayList<>();
        for (AssociationRules.Rule<String> rule : model.generateAssociationRules(minConfidence).toJavaRDD().collect()) {
            Rule ginimodel = new Rule(appmodelId, rule.javaAntecedent() + "", rule.javaConsequent() + "", rule.confidence() + "");
            System.out.println(rule.javaAntecedent() + "=>" + rule.javaConsequent() + ", " + rule.confidence());
            ruleList.add(ginimodel);
        }
        List<Rule> filterErrorResultList = null;
        try {
            filterErrorResultList = DbStoreHelper.filterErrorResult(ruleList);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        DbStoreHelper.insertList(filterErrorResultList);
    }
}
