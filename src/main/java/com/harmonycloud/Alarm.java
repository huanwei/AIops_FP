package com.harmonycloud;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class Alarm {
    private Integer id;

    private String subId;

    private String happendModel;


    private Date happendTime;


    private Integer type;


    private String relatedId;


    private Integer alarmLevel;


    private Integer alarmStatus;


    private String alarmContent;

    private Integer appmodelId;


    private String alarmDetail;

    public Alarm() {
        super();
    }

    public Alarm(String subId, String happendModel, Date happendTime, String alarmContent, String alarmDetail) {
        this.subId = subId;
        this.happendModel = happendModel;
        this.happendTime = happendTime;
        this.alarmContent = alarmContent;
        this.type = 1;
        this.relatedId = "";
        this.alarmLevel = getRandom();
        this.alarmStatus = getRandom();
        this.alarmDetail = alarmDetail;
    }


    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return sub_id
     */
    public String getSubId() {
        return subId;
    }

    /**
     * @param subId
     */
    public void setSubId(String subId) {
        this.subId = subId;
    }

    /**
     * 获取告警发生模块
     *
     * @return happend_model - 告警发生模块
     */
    public String getHappendModel() {
        return happendModel;
    }

    /**
     * 设置告警发生模块
     *
     * @param happendModel 告警发生模块
     */
    public void setHappendModel(String happendModel) {
        this.happendModel = happendModel;
    }

    /**
     * 获取告警发生时间
     *
     * @return happend_time - 告警发生时间
     */
    public Date getHappendTime() {
        return happendTime;
    }

    /**
     * 设置告警发生时间
     *
     * @param happendTime 告警发生时间
     */
    public void setHappendTime(Date happendTime) {
        this.happendTime = happendTime;
    }

    /**
     * 获取1为根源告警，0为衍生告警
     *
     * @return type - 1为根源告警，0为衍生告警
     */
    public Integer getType() {
        return type;
    }

    /**
     * 设置1为根源告警，0为衍生告警
     *
     * @param type 1为根源告警，0为衍生告警
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 获取告警关联ID
     *
     * @return related_id - 告警关联ID
     */
    public String getRelatedId() {
        return relatedId;
    }

    /**
     * 设置告警关联ID
     *
     * @param relatedId 告警关联ID
     */
    public void setRelatedId(String relatedId) {
        this.relatedId = relatedId;
    }

    /**
     * 获取告警等级
     *
     * @return alarm_level - 告警等级
     */
    public Integer getAlarmLevel() {
        return alarmLevel;
    }

    /**
     * 设置告警等级
     *
     * @param alarmLevel 告警等级
     */
    public void setAlarmLevel(Integer alarmLevel) {
        this.alarmLevel = alarmLevel;
    }

    /**
     * 获取告警状态
     *
     * @return alarm_status - 告警状态
     */
    public Integer getAlarmStatus() {
        return alarmStatus;
    }

    /**
     * 设置告警状态
     *
     * @param alarmStatus 告警状态
     */
    public void setAlarmStatus(Integer alarmStatus) {
        this.alarmStatus = alarmStatus;
    }

    /**
     * 获取告警内容
     *
     * @return alarm_content - 告警内容
     */
    public String getAlarmContent() {
        return alarmContent;
    }

    /**
     * 设置告警内容
     *
     * @param alarmContent 告警内容
     */
    public void setAlarmContent(String alarmContent) {
        this.alarmContent = alarmContent;
    }

    public Integer getAppmodelId() {
        return appmodelId;
    }

    public void setAppmodelId(Integer appmodelId) {
        this.appmodelId = appmodelId;
    }

    public String getAlarmDetail() {
        return alarmDetail;
    }

    public void setAlarmDetail(String alarmDetail) {
        this.alarmDetail = alarmDetail;
    }

    private int getRandom() {
        Random random = new Random();
        int rs = random.nextInt();
        if (rs % 4 <= 0)
            rs = 1;
        else rs = rs % 4;
        return rs;
    }

    @Override
    public String toString() {
        return "Alarm{" +
                "id=" + id +
                ", subId='" + subId + '\'' +
                ", happendModel='" + happendModel + '\'' +
                ", happendTime=" + happendTime +
                ", type=" + type +
                ", relatedId='" + relatedId + '\'' +
                ", alarmLevel=" + alarmLevel +
                ", alarmStatus=" + alarmStatus +
                ", alarmContent='" + alarmContent + '\'' +
                ", appmodelId=" + appmodelId +
                ", alarmDetail='" + alarmDetail + '\'' +
                '}';
    }
}